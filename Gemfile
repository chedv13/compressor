source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.1'
# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'compass-rails', '~> 2.0.4'
gem 'compass-normalize', '~> 1.5'

gem 'rails_12factor', group: :production

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  # gem 'spring'

  gem 'zeus'
end

gem 'oj'
gem 'coffee-script-source', '~> 1.9.1'
gem 'coffee-script', '>= 2.4.1'
gem 'spree', git: 'https://github.com/spree/spree.git', branch: '3-0-stable'
gem 'spree_gateway', github: 'spree/spree_gateway', branch: '3-0-stable'
gem 'spree_auth_devise', github: 'spree/spree_auth_devise', branch: '3-0-stable'
gem 'spree_multi_currency', github: 'spree-contrib/spree_multi_currency', branch: '3-0-stable'
gem 'spree_i18n', github: 'spree-contrib/spree_i18n', branch: '3-0-stable'
gem 'friendly_id', '~> 5.1.0'
gem 'friendly_id-globalize'
gem 'slim-rails', '~> 3.0.1'
gem 'high_voltage', '~> 2.2.1'

gem 'datashift', github: 'autotelik/datashift', branch: 'master'
gem 'datashift_spree', github: 'autotelik/datashift_spree', branch: 'master'

gem 'active_model_serializers', '~> 0.9.3'
gem 'semantic-ui-sass', github: 'doabit/semantic-ui-sass'