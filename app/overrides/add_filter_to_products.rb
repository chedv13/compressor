Deface::Override.new(:virtual_path => 'spree/shared/_products',
                     :name => 'Add filters to products view',
                     :insert_before => '[id="products-display-manager"]',
                     :partial => 'spree/products/filters',
                     :disabled => false)