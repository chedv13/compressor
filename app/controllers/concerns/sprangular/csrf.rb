module ObservableRoles
  module Subscriber
    def self.included(base)
      attr_accessor :subscrbr_lock
      attr_reader :captured_observable_events

      base.extend(ClassMethods)
    end

    module ClassMethods
      attr_accessor :observed_publisher_callbacks # здесь описанные методы можно вынести в attr_accessor и неправильное наименование методов
    end

    # Captures observable events. Исправил наименование
    def capture_observable_event(role, event_name, data={})
      return if role.nil? || event_name.nil?

      role = role.to_sym
      event_name = event_name.to_sym

      if self.class.observed_publisher_callbacks.nil? ||
          self.class.observed_publisher_callbacks[role].nil? || self.class.observed_publisher_callbacks[role][event_name].nil?
        return
      end

      # Если уж совсем по-честному, то я бы еще добавил проверку на то есть ли в классе метод, на который мы делаем callback

      @captured_observable_events ||= []
      @captured_observable_events.push({ callback: self.class.observed_publisher_callbacks[role][event_name], data: data })

      release_captured_events unless @subscrbr_lock
    end

    private

    # releases captured events. Исправил наименование
    def release_captured_events
      @subscrbr_lock = true

      # Заменил while ! на until - более правильное решение
      until @captured_observable_events.empty?
        e = @captured_observable_events.shift

        e[:callback].call(self, e[:data])
      end

      @subscrbr_lock = false
    end
  end

  module Publisher
    def self.included(base)
      attr_accessor :role
    end

    def subscribe(s)
      @observing_subscribers = [] unless @observing_subscribers
      @observing_subscribers << s
    end

    def unsubscribe(s)
      unless @observing_subscribers.nil?
        @observing_subscribers.delete(s)
      end
    end

    def publish_event(event_name, data={})
      return unless @observing_subscribers

      @observing_subscribers.each do |s|
        if !block_given? || yield(s)
          s.capture_observable_event(@role, event_name, data)
        end
      end
    end
  end
end

class MySubscriber
  include ObservableRoles::Subscriber
end

my_subscriber = MySubscriber.new

callback = lambda { |obj, data| p obj.class.name }

MySubscriber.observed_publisher_callbacks = { my: { my_event: callback }, not_my: { not_my_event: 'my_subscriber2' } }

class MyPublisher
  include ObservableRoles::Publisher
end

my_publisher = MyPublisher.new
my_publisher.role = 'my'
my_publisher.subscribe(my_subscriber)
my_publisher.publish_event('my_event')