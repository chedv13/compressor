module Spree
  module Api
    class PricesController < Spree::Api::BaseController
      def extreme
        render json: Spree::Price.extreme(params[:permalink])
      end
    end
  end
end
