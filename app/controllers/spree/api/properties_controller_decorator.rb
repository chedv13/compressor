Spree::Api::PropertiesController.class_eval do
  def extreme
    render json: params[:names].split(',').inject({}) { |result, name|
             property = Spree::Property.where(name: name).first

             if property
               extremes = property.extremes(params[:permalink])

               result[name.downcase] = extremes ? { max: extremes[1], min: extremes[0] } : { max: nil, min: nil }
             end

             result
           }
  end
end
