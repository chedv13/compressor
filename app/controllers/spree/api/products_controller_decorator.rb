Spree::Api::ProductsController.class_eval do
  include Spree::Core::ControllerHelpers::Order
  include Spree::BaseHelper
  include ActionView::Helpers::AssetTagHelper

  def index
    conditions = "spree_prices.currency = 'RUB'"

    if params[:additional_query]
      if params[:additional_query][:name]
        conditions += " AND spree_product_translations.name ILIKE '%#{params[:additional_query][:name]}%'"
      end

      if params[:additional_query][:properties]
        params[:additional_query][:properties].each_pair do |property_name, values|
          capitalize_property_name = property_name.capitalize

          conditions += " AND (
                  SELECT TRUE
                  FROM spree_product_properties
                    INNER JOIN spree_property_translations
                      ON spree_product_properties.property_id = spree_property_translations.spree_property_id
                    INNER JOIN spree_product_property_translations
                      ON spree_product_property_translations.spree_product_property_id = spree_product_properties.id
                  WHERE spree_property_translations.name = '#{capitalize_property_name}'
                        AND spree_product_properties.product_id = spree_products.id
                        AND CASE spree_property_translations.name
                            WHEN '#{capitalize_property_name}'
                              THEN
                                reverse( substring(
                                  reverse(
                                      replace(
                                          replace( substring( spree_product_property_translations.value FROM 4 ), '''',
                                                   '' ), ' ', '' ) ) FROM 2 ) ) :: FLOAT >= #{values['min']}
                            ELSE TRUE END
                        AND CASE spree_property_translations.name
                            WHEN '#{capitalize_property_name}'
                              THEN
                                reverse( substring(
                                  reverse(
                                      replace(
                                          replace( substring( spree_product_property_translations.value FROM 4 ), '''',
                                                   '' ), ' ', '' ) ) FROM 2 ) ) :: FLOAT <= #{values['max']}
                            ELSE TRUE END
                )"
        end
      end

      if params[:additional_query][:amount]
        conditions += " AND spree_prices.amount >= #{params[:additional_query][:amount][:min]} AND spree_prices.amount <= #{params[:additional_query][:amount][:max]}"
      end
    end

    if params[:taxon] && params[:taxon][:permalink]
      conditions += " AND spree_products_taxons.taxon_id IN (
        SELECT id
        FROM spree_taxons
        WHERE lft >=
              (
                SELECT lft
                FROM spree_taxons
                WHERE id = (
                  SELECT spree_taxon_translations.spree_taxon_id
                  FROM spree_taxon_translations
                  WHERE spree_taxon_translations.permalink = '#{params[:taxon][:permalink]}')) AND rgt <= (
          SELECT rgt
          FROM spree_taxons
          WHERE id = (
            SELECT spree_taxon_translations.spree_taxon_id
            FROM spree_taxon_translations
            WHERE spree_taxon_translations.permalink = '#{params[:taxon][:permalink]}'))
      )"
    end

    @products = product_scope
                    .select('spree_products.id')
                    .joins('
                      INNER JOIN spree_product_translations ON spree_product_translations.spree_product_id = spree_products.id
                      INNER JOIN spree_products_taxons ON spree_products.id = spree_products_taxons.product_id
                      INNER JOIN spree_taxon_translations ON spree_taxon_translations.spree_taxon_id = spree_products_taxons.taxon_id
                    ')
                    .where(conditions)
                    .distinct.page(params[:page]).per(params[:per_page])

    count = @products.count
    total_count = @products.total_count
    pages = @products.num_pages

    select_fields = '
                      spree_products.id,
                      spree_variants.id AS variant_id,
                      spree_prices.amount,
                      spree_product_translations.name,
                      spree_variants.sku
                    '

    if params[:properties]
      select_fields += ',(
        SELECT json_agg( row_to_json( t ) )
           FROM (
            SELECT
              spree_property_translations.name,
      '

      Rails.logger.info params

      if params[:properties][:text]
        select_fields += %{
          CASE
            WHEN spree_property_translations.name IN (#{params[:properties][:text].split(',').map { |x| "'" + x + "'" }.join(',')})
              THEN
                reverse(
                  substring(
                    reverse(
                      replace(
                        replace(
                          substring( spree_product_property_translations.value FROM 4 ), '''', ''), ' ', '')) FROM 6))
            ELSE
              reverse(
                substring(
                  reverse(
                    replace(
                      replace(
                        substring(
                          spree_product_property_translations.value FROM 4 ), '''', '' ), ' ', '')) FROM 2))
            END AS value
        }
      else
        select_fields += %{
          reverse(
            substring(
              reverse(
                replace(
                  replace(
                    substring( spree_product_property_translations.value FROM 4 ), '''', ''), ' ', '')) FROM 2)) AS value
        }
      end

      select_fields += %{
            FROM spree_product_properties
              INNER JOIN spree_property_translations ON spree_property_translations.spree_property_id = spree_product_properties.property_id
              INNER JOIN spree_product_property_translations ON spree_product_properties.id = spree_product_property_translations.spree_product_property_id
            WHERE spree_product_properties.product_id = spree_products.id
              #{params[:properties] ? " AND spree_property_translations.name IN (#{params[:properties].values.join(',').split(',').map { |x| "'" + x + "'" }.join(',')})" : ''}
          ) t
        ) AS properties_json
      }
    end

    @products = product_scope
                    .select(select_fields)
                    .joins('
                      INNER JOIN spree_product_translations ON spree_product_translations.spree_product_id = spree_products.id
                      INNER JOIN spree_products_taxons ON spree_products.id = spree_products_taxons.product_id
                      INNER JOIN spree_taxon_translations ON spree_taxon_translations.spree_taxon_id = spree_products_taxons.taxon_id
                    ')
                    .where(conditions)
                    .page(params[:page])
                    .per(params[:per_page])
                    .group('spree_products.id, spree_prices.amount, spree_product_translations.name, spree_variants.sku, spree_variants.id')
                    .order(
                        if params[:order_type] == 'field'
                          "#{params[:order_name]} #{params[:order_direction]}"
                        else
                          "(
                            SELECT
                              reverse( substring(
                                reverse(
                                   replace( replace( substring( spree_product_property_translations.value FROM 4 ), '''', '' ),
                                             ' ', '' ) ) FROM 2 ) ) :: FLOAT
                            FROM spree_product_properties
                               INNER JOIN spree_property_translations
                                 ON spree_property_translations.spree_property_id = spree_product_properties.property_id
                              INNER JOIN spree_product_property_translations
                                 ON spree_product_properties.id = spree_product_property_translations.spree_product_property_id
                             WHERE spree_product_properties.product_id = spree_products.id AND spree_property_translations.name = '#{params[:order_name].capitalize}'
                          ) #{params[:order_direction]}"
                        end
                    )


    expires_in 15.minutes, :public => true
    headers['Surrogate-Control'] = "max-age=#{15.minutes}"

    render json: {
               count: count,
               total_count: total_count,
               current_page: (params[:page] ? params[:page].to_i : 1),
               per_page: (params[:per_page] || Kaminari.config.default_per_page),
               pages: pages,
               products: @products.map do |product|
                 {
                     properties: {}, # {}.tap { |hash| product.properties_json.each { |x| hash[x['name'].downcase] = x['value'] } },
                     description: product.description,
                     display_price: "#{product.amount} ₽",
                     id: product.id,
                     name: product.name,
                     sku: product.sku,
                     variant_id: product.variant_id,
                     images: {
                         small: product_image(product, itemprop: 'image')
                     }
                     # small_url: nil # product.images.first.attachment.url(:product, timestamp: false)
                 }
               end
           }
  end

  def product_properties
    render json: { properties: Spree::Product.product_properties(params[:taxon_permalink]) }
  end
end
