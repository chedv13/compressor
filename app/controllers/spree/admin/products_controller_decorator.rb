Spree::Admin::ProductsController.class_eval do
  helper 'spree/products'

  before_action :load_data, except: :index
  create.before :create_before
  update.before :update_before
  helper_method :clone_object_url

  def show
    session[:return_to] ||= request.referer
    redirect_to action: :edit
  end

  def index
    session[:return_to] = request.url
    respond_with(@collection)
  end

  def update
    if params[:product][:taxon_ids].present?
      params[:product][:taxon_ids] = params[:product][:taxon_ids].split(',')
    end
    if params[:product][:option_type_ids].present?
      params[:product][:option_type_ids] = params[:product][:option_type_ids].split(',')
    end
    invoke_callbacks(:update, :before)
    # if permitted_resource_params[:product_properties_attributes]
    #   permitted_resource_params[:product_properties_attributes].each_pair do |seq_number, product_property_hash|
    #     if Spree::ProductProperty.exists?(product_id: @object.id, property_id: product_property_hash['id'])
    #       product_property = Spree::ProductProperty.where(product_id: @object.id, property_id: product_property_hash['id']).first
    #
    #       product_property.update_attribute(:value, product_property_hash['value'])
    #     else
    #       Spree::ProductProperty.create!(product_id: @object.id, property: Spree::Property.find(product_property_hash['id']), value: product_property_hash['value'])
    #     end
    #   end
    #   permitted_resource_params.delete(:product_properties_attributes)
    #
    #   invoke_callbacks(:update, :after)
    #   flash[:success] = flash_message_for(@object, :successfully_updated)
    #   respond_with(@object) do |format|
    #     format.html { redirect_to "/admin/products/#{@object.slug}/edit" }
    #     format.js { render layout: false }
    #   end
    # end
    # unless permitted_resource_params.empty?
    if @object.update_attributes(permitted_resource_params)
      invoke_callbacks(:update, :after)
      flash[:success] = flash_message_for(@object, :successfully_updated)
      respond_with(@object) do |format|
        format.html { redirect_to location_after_save }
        format.js { render layout: false }
      end
    else
      # Stops people submitting blank slugs, causing errors when they try to
      # update the product again
      @product.slug = @product.slug_was if @product.slug.blank?
      invoke_callbacks(:update, :fails)
      respond_with(@object)
    end
    # end
  end

  def destroy
    @product = Spree::Product.friendly.find(params[:id])
    @product.destroy

    flash[:success] = Spree.t('notice_messages.product_deleted')

    respond_with(@product) do |format|
      format.html { redirect_to collection_url }
      format.js { render_js_for_destroy }
    end
  end

  def clone
    @new = @product.duplicate

    if @new.save
      flash[:success] = Spree.t('notice_messages.product_cloned')
    else
      flash[:error] = Spree.t('notice_messages.product_not_cloned')
    end

    redirect_to edit_admin_product_url(@new)
  end

  def stock
    @variants = @product.variants.includes(*variant_stock_includes)
    @variants = [@product.master] if @variants.empty?
    @stock_locations = Spree::StockLocation.accessible_by(current_ability, :read)
    if @stock_locations.empty?
      flash[:error] = Spree.t(:stock_management_requires_a_stock_location)
      redirect_to admin_stock_locations_path
    end
  end

  protected

  def find_resource
    Spree::Product.with_deleted.friendly.find(params[:id])
  end

  def location_after_save
    spree.edit_admin_product_url(@product)
  end

  def load_data
    @taxons = Spree::Taxon.order(:name)
    @option_types = Spree::OptionType.order(:name)
    @tax_categories = Spree::TaxCategory.order(:name)
    @shipping_categories = Spree::ShippingCategory.order(:name)
  end

  def collection
    return @collection if @collection.present?
    params[:q] ||= {}
    params[:q][:deleted_at_null] ||= "1"

    params[:q][:s] ||= "name asc"
    @collection = super
    if params[:q].delete(:deleted_at_null) == '0'
      @collection = @collection.with_deleted
    end
    # @search needs to be defined as this is passed to search_form_for
    @search = @collection.ransack(params[:q])
    @collection = @search.result.
        distinct_by_product_ids(params[:q][:s]).
        includes(product_includes).
        page(params[:page]).
        per(params[:per_page] || Spree::Config[:admin_products_per_page])

    @collection
  end

  def create_before
    return if params[:product][:prototype_id].blank?
    @prototype = Spree::Prototype.find(params[:product][:prototype_id])
  end

  def update_before
    # note: we only reset the product properties if we're receiving a post
    #       from the form on that tab
    return unless params[:clear_product_properties]
    params[:product] ||= {}
  end

  def product_includes
    [{ variants: [:images], master: [:images, :default_price] }]
  end

  def clone_object_url(resource)
    clone_admin_product_url resource
  end

  private

  def variant_stock_includes
    [:images, stock_items: :stock_location, option_values: :option_type]
  end
end