Spree::Price.class_eval do
  def self.extreme(permalink = nil)
    params = {
        'spree_variants.deleted_at' => nil,
        'spree_variants.is_master' => 't'
    }

    if permalink
      conditions =<<HERE
        spree_products_taxons.taxon_id IN (
          SELECT id
            FROM spree_taxons
            WHERE lft >=
                  (
                    SELECT lft
                    FROM spree_taxons
                    WHERE id = (
                      SELECT spree_taxon_translations.spree_taxon_id
                      FROM spree_taxon_translations
                      WHERE spree_taxon_translations.permalink = '#{permalink}')) AND rgt <= (
              SELECT rgt
              FROM spree_taxons
              WHERE id = (
                SELECT spree_taxon_translations.spree_taxon_id
                FROM spree_taxon_translations
                WHERE spree_taxon_translations.permalink = '#{permalink}'))
        )
HERE

      params['spree_variants.product_id'] = Spree::Classification.joins('INNER JOIN spree_taxon_translations ON spree_taxon_translations.spree_taxon_id = spree_products_taxons.taxon_id').where(conditions).map(&:product_id)
    end

    {
        max: self.joins(:variant).where(params).maximum(:amount).to_f,
        min: self.joins(:variant).where(params).minimum(:amount).to_f
    }
  end
end