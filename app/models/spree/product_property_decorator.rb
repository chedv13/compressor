Spree::ProductProperty.class_eval do
  def property_name=(name)
    unless name.blank?
      unless property = Spree::Property.where(name: name).first
        property = Spree::Property.create(name: name, presentation: name)
      end
      self.property = property
    end
  end
end