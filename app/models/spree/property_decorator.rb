Spree::Property.class_eval do
  @@default_data_types = %w(int float text)

  cattr_reader :default_data_types

  def taxon_product_properties(permalink)
    conditions =<<HERE
        taxon_id IN (
          SELECT id
          FROM spree_taxons
          WHERE lft >=
                (
                  SELECT lft
                  FROM spree_taxons
                  WHERE id = (
                    SELECT spree_taxon_translations.spree_taxon_id
                    FROM spree_taxon_translations
                    WHERE spree_taxon_translations.permalink = '#{permalink}')) AND rgt <= (
            SELECT rgt
            FROM spree_taxons
            WHERE id = (
              SELECT spree_taxon_translations.spree_taxon_id
              FROM spree_taxon_translations
              WHERE spree_taxon_translations.permalink = '#{permalink}')))
HERE

    product_properties.where(product_id: Spree::Product.select('spree_products.id').joins('INNER JOIN spree_products_taxons ON spree_products_taxons.product_id = spree_products.id').where(conditions))
  end

  def extremes(permalink = nil)
    selected_product_properties = permalink ? taxon_product_properties(permalink) : product_properties

    selected_product_properties.size > 0 ? [selected_product_properties.min_by { |x| x.value.to_f }.value.to_f, selected_product_properties.max_by { |x| x.value.to_f }.value.to_f] : nil
  end

  def max(permalink = nil)
    selected_product_properties = permalink ? taxon_product_properties(permalink) : product_properties

    selected_product_properties.size > 0 ? selected_product_properties.max_by { |x| x.value.to_f }.value.to_f : nil
  end

  def min(permalink = nil)
    selected_product_properties = permalink ? taxon_product_properties(permalink) : product_properties

    selected_product_properties.size > 0 ? selected_product_properties.min_by { |x| x.value.to_f }.value.to_f : nil
  end
end