Spree::Taxon.class_eval do
  def max_price
    Spree::Variant.joins(:prices).where(
        deleted_at: nil,
        is_master: 't',
        product_id: Spree::Classification.where(taxon_id: self.id).map(&:product_id)
    ).maximum(:amount).to_f
  end

  def min_price
    Spree::Variant.joins(:prices).where(
        deleted_at: nil,
        is_master: 't',
        product_id: Spree::Classification.where(taxon_id: self.id).map(&:product_id)
    ).minimum(:amount).to_f
  end
end