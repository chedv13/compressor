Spree::Product.class_eval do
  def self.product_properties(taxon_permalink = nil)
    query = '
      SELECT json_agg( row_to_json( properties ) )
        FROM (
          SELECT
            property_id,
            (
              SELECT data_type
                FROM spree_properties
                WHERE spree_properties.id = property_id),
            (
              SELECT name
                FROM spree_property_translations
                WHERE spree_property_translations.spree_property_id = property_id
            )
            FROM (
              SELECT *
              FROM unnest( (
                 SELECT COALESCE(int_array_intersect_agg( COALESCE( (
                     SELECT array_agg( property_id )
                     FROM
                       (
                         SELECT
                           spree_product_properties.property_id
                         FROM
                            spree_product_properties
                              INNER JOIN spree_property_translations  ON spree_property_translations.spree_property_id = spree_product_properties.property_id
                         WHERE
                           spree_product_properties.product_id = spree_products.id) AS t),
                        ARRAY [] :: INT [] ) ),
                      ARRAY [] :: INT [] ) AS property_names
                  FROM spree_products
    '

    if taxon_permalink
      query += %{
        INNER JOIN spree_products_taxons ON spree_products_taxons.product_id = spree_products.id
        INNER JOIN spree_taxon_translations ON spree_taxon_translations.spree_taxon_id = spree_products_taxons.taxon_id
        WHERE permalink = '#{taxon_permalink}'
      }
    end

    query += ') ) AS property_id) AS property_id_rows) AS properties'

    result_value = ActiveRecord::Base.connection.execute(query).values[0][0]

    result_value ? JSON.parse(result_value).group_by { |property| property['data_type'] } : []
  end

  def max_option_val(property_name)
    property = Spree::Property.where(name: property_name).first

    property ? Spree::ProductProperty.where(product_id: Spree::Taxon.where(name: name).first.products, property: property).max_by { |x| x.value.to_i } : nil
  end

  def max_price
    Spree::Variant.joins(:prices).where(
        deleted_at: nil,
        is_master: 't',
        product_id: Spree::Classification.where(taxon_id: self.id).map(&:product_id)
    ).maximum(:amount).to_f
  end

  def min_option_val(property_name)
    property = Spree::Property.where(name: property_name).first

    property ? Spree::ProductProperty.where(product_id: Spree::Taxon.where(name: name).first.products, property: property).min_by { |x| x.value.to_i } : nil
  end

  def min_price
    Spree::Variant.joins(:prices).where(
        deleted_at: nil,
        is_master: 't',
        product_id: Spree::Classification.where(taxon_id: self.id).map(&:product_id)
    ).minimum(:amount).to_f
  end

  def property(property_name)
    return nil unless prop = properties.where(name: property_name).first

    product_properties.where(property: prop).first.try(:value)
  end

  private

  def add_associations_from_prototype
    if prototype_id && prototype = Spree::Prototype.where(id: prototype_id).first
      prototype.properties.each do |property|
        product_properties.create(property: property)
      end
      self.option_types = prototype.option_types
      self.taxons = prototype.taxons
    end
  end
end