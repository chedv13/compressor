Spree::Image.class_eval do
  has_attached_file :attachment,
                    styles: {
                        mini: '48x48^',
                        small: '100x100^',
                        product: '240x240^',
                        large: '600x600^'
                    },
                    default_style: :product,
                    url: '/spree/products/:id/:style/:basename.:extension',
                    path: ':rails_root/public/spree/products/:id/:style/:basename.:extension',
                    convert_options: {
                        mini: " -gravity center -crop '48x48+0+0' -strip -auto-orient -colorspace sRGB",
                        small: " -gravity center -crop '100x100+0+0' -strip -auto-orient -colorspace sRGB",
                        product: " -gravity center -crop '240x240+0+0' -strip -auto-orient -colorspace sRGB",
                        large: " -gravity center -crop '600x600+0+0' -strip -auto-orient -colorspace sRGB"
                    }
end