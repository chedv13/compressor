json.count @products.count
json.total_count @products.total_count
json.current_page (params[:page] ? params[:page].to_i : 1)
json.per_page (params[:per_page] || Kaminari.config.default_per_page)
json.pages @products.num_pages
json.products do |product|
  json.partial! partial: 'spree/api/products/show.v2', product: product
end
