cache [I18n.locale, @current_user_roles.include?('admin'), current_currency, root_object]

json.display_price product.display_price.to_s
json.has_variants product.has_variants?
json.taxon_ids product.taxon_ids

# child :master => :master do
#   extends "spree/api/variants/small"
# end
#
# child :variants => :variants do
#   extends "spree/api/variants/small"
# end
#
# child :option_types => :option_types do
#   attributes *option_type_attributes
# end
#
# child :product_properties => :product_properties do
#   attributes *product_property_attributes
# end
#
# child :classifications => :classifications do
#   attributes :taxon_id, :position
#
#   child(:taxon) do
#     extends "spree/api/taxons/show"
#   end
# end
