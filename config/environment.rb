# Load the Rails application.
require File.expand_path('../application', __FILE__)
require 'datashift_spree/loaders/spree/product_loader'

# Initialize the Rails application.
Rails.application.initialize!
