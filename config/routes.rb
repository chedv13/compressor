Rails.application.routes.draw do
  get '/info', to: 'spree/info#index', as: :info
  get '/contacts', to: 'spree/info#contacts', as: :contacts

  mount Spree::Core::Engine, :at => '/'

  scope module: 'sprangular' do
    root to: 'home#index'
    # resources "locale", only: :show, param: :locale

    scope '/api', defaults: { format: :json } do
      resources :taxonomies, only: :index
      get 'taxons/*permalink', to: 'taxons#show'
      resources :products, only: %i(index show)
      resource :cart do
        post :add_variant
        post :guest_login
        put :update_variant
        put :change_variant
        put :remove_adjustment
        delete :remove_variant
      end
      resource :account
      resources :passwords
      resources :credit_cards, only: :destroy
      resources :addresses, only: :destroy
      resources :countries, only: :index
      resources :orders, only: %i(index show)
    end
  end
end

Spree::Core::Engine.routes.draw do
  namespace :api, defaults: { format: :json } do
    get 'prices/extreme' => 'prices#extreme'
    get 'properties/extreme' => 'properties#extreme'
    get 'products/properties' => 'products#product_properties'
  end
end

# Spree::Core::Engine.routes.draw do
#   scope module: 'sprangular' do
#     root to: 'home#index'
#     resources "locale", only: :show, param: :locale
#
#     scope '/api', defaults: { format: :json } do
#       resources :taxonomies, only: :index
#       get 'taxons/*permalink', to: 'taxons#show'
#       resources :products, only: %i(index show)
#       resource :cart do
#         post :add_variant
#         post :guest_login
#         put :update_variant
#         put :change_variant
#         put :remove_adjustment
#         delete :remove_variant
#       end
#       resource :account
#       resources :passwords
#       resources :credit_cards, only: :destroy
#       resources :addresses, only: :destroy
#       resources :countries, only: :index
#       resources :orders, only: %i(index show)
#     end
#   end
# end
