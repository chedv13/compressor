class AddDataTypeToSpreeProperty < ActiveRecord::Migration
  def change
    add_column :spree_properties, :data_type, :string
  end
end
