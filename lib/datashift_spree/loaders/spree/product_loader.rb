DataShift::SpreeEcom::ProductLoader.class_eval do
  def process(method_detail, value)

    raise ProductLoadError.new("Cannot process #{value} NO details found to assign to") unless (method_detail)

    # TODO - start supporting assigning extra data via current_attribute_hash
    current_value, current_attribute_hash = @populator.prepare_data(method_detail, value)

    current_method_detail = method_detail

    logger.debug "Processing value: [#{current_value}]"

    # Special cases for Products, generally where a simple one stage lookup won't suffice
    # otherwise simply use default processing from base class
    if (current_value && (current_method_detail.operator?('variants') || current_method_detail.operator?('option_types')))

      add_options_variants

    elsif (current_method_detail.operator?('taxons') && current_value)

      add_taxons

    elsif (current_method_detail.operator?('product_properties'))

      add_properties

      # This loads images to Product or Product Master Variant depending on Spree version
    elsif (current_method_detail.operator?('images') && current_value)

      add_images(load_object.master)

      # This loads images to Product Variants
    elsif (current_method_detail.operator?('variant_images') && current_value)

      add_variant_images(current_value)

    elsif (current_method_detail.operator?('available_on') && current_value)

      @load_object.available_on = Time.parse(current_value)
      @load_object.save

    elsif (current_method_detail.operator?('price') && current_value)

      @load_object.price = current_value.to_f
      @load_object.save

    elsif (current_method_detail.operator?('variant_price') && current_value)

      if (@load_object.variants.size > 0)

        if (current_value.to_s.include?(Delimiters::multi_assoc_delim))

          # Check if we processed Option Types and assign  per option
          values = current_value.to_s.split(Delimiters::multi_assoc_delim)

          if (@load_object.variants.size == values.size)
            @load_object.variants.each_with_index { |v, i| v.price = values[i].to_f }
            @load_object.save
          else
            puts "WARNING: Price entries did not match number of Variants - None Set"
          end
        end

      else
        super
      end

    elsif (current_method_detail.operator?('variant_cost_price') && current_value)

      if (@load_object.variants.size > 0)

        if (current_value.to_s.include?(Delimiters::multi_assoc_delim))

          # Check if we processed Option Types and assign  per option
          values = current_value.to_s.split(Delimiters::multi_assoc_delim)

          if (@load_object.variants.size == values.size)
            @load_object.variants.each_with_index { |v, i| v.cost_price = values[i].to_f }
            @load_object.save
          else
            puts "WARNING: Cost Price entries did not match number of Variants - None Set"
          end
        end

      else
        super
      end

    elsif (current_method_detail.operator?('variant_sku') && current_value)

      if (@load_object.variants.size > 0)

        if (current_value.to_s.include?(Delimiters::multi_assoc_delim))

          # Check if we processed Option Types and assign  per option
          values = current_value.to_s.split(Delimiters::multi_assoc_delim)

          if (@load_object.variants.size == values.size)
            @load_object.variants.each_with_index { |v, i| v.sku = values[i].to_s }
            @load_object.save
          else
            puts "WARNING: SKU entries did not match number of Variants - None Set"
          end
        end

      else
        super
      end

      #elsif(current_value && (current_method_detail.operator?('count_on_hand') || current_method_detail.operator?('on_hand')) )
    elsif (current_value && current_method_detail.operator?('stock_items'))

      logger.info "Adding Variants Stock Items (count_on_hand)"

      save_if_new

      add_variants_stock(current_value)

    else
      super
    end
  end

end
