// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require semantic-ui
//= require spree/frontend
//= require spree/frontend/spree_multi_currency
//= require ../../custom/validity
//= require ../../custom/angular.min
//= require ../../custom/angular-resource.min
//= require ../../custom/angular-route.min
//= require ../../custom/angular-animate.min
//= require ../../custom/angular-sanitize.min
//= require ../../custom/angular-translate.min
//= require ../../custom/angular-slider
//= require ../../custom/slider
//= require ../../custom/ui-bootstrap-tpls-0.13.0.min
//= require ../../custom/lodash.min
//= require ../../custom/ya-map-2.1.min
//= require ../../custom/app
//= require ../../custom/translations
//= require ../../custom/helpers/underscore
//= require ../../custom/interceptors/loading
//= require ../../custom/models/address
//= require ../../custom/models/adjustment
//= require ../../custom/models/creditCard
//= require ../../custom/models/image
//= require ../../custom/models/order
//= require ../../custom/models/product
//= require ../../custom/models/shippingRate
//= require ../../custom/models/user
//= require ../../custom/models/variant
//= require ../../custom/providers/product
//= require ../../custom/controllers/authorization
//= require ../../custom/controllers/cart
//= require ../../custom/controllers/catalog
//= require ../../custom/controllers/checkout
//= require ../../custom/controllers/common
//= require ../../custom/controllers/contacts
//= require ../../custom/controllers/information
//= require ../../custom/controllers/orders
//= require ../../custom/controllers/payments_info
//= require ../../custom/controllers/product
//= require ../../custom/controllers/registration
//= require ../../custom/controllers/services
//= require ../../custom/controllers/settings
//= require ../../custom/services/account
//= require ../../custom/services/cart
//= require ../../custom/services/flash
//= require ../../custom/services/status
