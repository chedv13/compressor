//Compressor.run(function ($translate) {
//    var i, key, keys, len, results, translate;
//    keys = ["required", "greaterThan", "greaterThanOrEqual", "lessThan", "lessThanOrEqual", "regex", "length", "lengthGreaterThan", "lengthLessThan", "number"];
//    translate = function (key) {
//        return $translate("validation." + key).then(function (text) {
//            return Validity.MESSAGES[key] = text;
//        });
//    };
//    results = [];
//    for (i = 0, len = keys.length; i < len; i++) {
//        key = keys[i];
//        results.push(translate(key));
//    }
//    return results;
//});