'use strict';

Compressor.config(function ($provide, $httpProvider) {
    $provide.factory('loadingInterceptor', function ($q, Status) {
        return {
            request: function (config) {
                if (!config.ignoreLoadingIndicator) {
                    Status.httpLoading = true;
                }
                return config;
            },
            response: function (response) {
                Status.httpLoading = false;
                return response;
            },
            responseError: function (rejection) {
                Status.httpLoading = false;
                return $q.reject(rejection);
            }
        };
    });
    return $httpProvider.interceptors.push('loadingInterceptor');
});