angular.module('compressorApp')
    .provider('Price', function () {
        this.$get = ['$resource', function ($resource) {
            return $resource('/api/products/:_id', {}, {
                update: {
                    method: 'PUT'
                },
                query: {
                    method: 'GET',
                    isArray: false
                }
            });
        }];
    });