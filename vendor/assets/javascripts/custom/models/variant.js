Compressor.Variant = (function() {
    function Variant() {}

    Variant.prototype.init = function() {
        this.images = Compressor.extend(this.images, Compressor.Image);
        return this.image = this.images[0];
    };

    Variant.prototype.isAvailable = function() {
        return !this.track_inventory || this.in_stock || this.is_backorderable;
    };

    return Variant;

})();