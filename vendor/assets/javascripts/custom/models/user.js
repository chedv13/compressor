Compressor.User = (function() {
    function User() {}

    User.prototype.init = function() {
        this.creditCards = Compressor.extend(this.payment_sources || [], Compressor.CreditCard);
        this.addresses = Compressor.extend(this.addresses || [], Compressor.Address);
        if (this.bill_address) {
            this.billingAddress = Compressor.extend(this.bill_address, Compressor.Address);
        }
        if (this.ship_address) {
            return this.shippingAddress = Compressor.extend(this.ship_address, Compressor.Address);
        }
    };

    User.prototype.serialize = function() {
        return _.omit(this, function(value) {
            return typeof value === 'object' || typeof value === 'function' || Array.isArray(value);
        });
    };

    User.prototype.findAddress = function(id) {
        return _.find(this.addresses, function(address) {
            return address.id === id;
        });
    };

    return User;

})();