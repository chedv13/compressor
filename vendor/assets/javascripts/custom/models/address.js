'use strict';
Compressor.Address = (function() {
    function Address() {}

    Validity.define(Address, {
        firstname: 'required',
        lastname: 'required',
        address1: 'required',
        city: 'required',
        state: ['_validateState'],
        country: 'required',
        zipcode: 'required',
        phone: 'required'
    });

    Address.prototype.init = function() {
        this.stateId = this.state_id;
        return this.countryId = this.country_id;
    };

    Address.prototype.fullName = function() {
        return this.firstname + " " + this.lastname;
    };

    Address.prototype.shortAddress = function() {
        return (this.fullName()) + ", " + (this.addressLine());
    };

    Address.prototype.actualStateName = function() {
        var ref;
        return ((ref = this.state) != null ? ref.abbr : void 0) || this.state_name;
    };

    Address.prototype.addressLine = function() {
        if (this.address2) {
            return this.address1 + " " + this.address2;
        } else {
            return this.address1;
        }
    };

    Address.prototype.serialize = function() {
        return {
            firstname: this.firstname,
            lastname: this.lastname,
            address1: this.address1,
            address2: this.address2,
            city: this.city,
            phone: this.phone,
            zipcode: this.zipcode,
            state_id: this.stateId,
            state_name: this.state_name,
            country_id: this.countryId
        };
    };

    Address.prototype.isEmpty = function() {
        return !this.firstname && !this.lastname && !this.address1 && !this.address2 && !this.city && !this.phone && !this.zipcode && !this.countryId && !this.stateId;
    };

    Address.prototype.same = function(other) {
        if (!other) {
            return;
        }
        return this.firstname === other.firstname && this.lastname === other.lastname && this.address1 === other.address1 && this.address2 === other.address2 && this.city === other.city && this.phone === other.phone && this.zipcode === other.zipcode && this.countryId === other.countryId && this.stateId === other.stateId;
    };

    Address.prototype.key = function() {
        return [this.firstname, this.lastname, this.address1, this.address2, this.city, this.phone, this.zipcode, this.countryId, this.stateId].join('');
    };

    Address.prototype._validateState = function() {
        if ((this.country && this.country.states_required) && !this.actualStateName()) {
            return "can't be blank";
        }
    };

    return Address;

})();