angular.module('compressorApp')
    .factory('ShippingRate', function () {
        function ShippingRate() {
        }

        ShippingRate.prototype.init = function () {
            this.shippingMethodId = this.shipping_method_id;
            return this.cost = Number(this.cost);
        };

        return ShippingRate;
    });