Compressor.Adjustment = (function () {
    function Adjustment() {
    }

    Adjustment.prototype.init = function () {
        return this.amount = Number(this.amount);
    };

    Adjustment.prototype.isPromo = function () {
        return this.source_type === 'Spree::PromotionAction';
    };

    Adjustment.prototype.promoCode = function () {
        return this.label.split(/[()]+/)[1];
    };

    return Adjustment;
})();