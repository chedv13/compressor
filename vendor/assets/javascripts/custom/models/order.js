'use strict';
Compressor.Order = (function () {
    function Order() {
        this.creditCard = new Compressor.CreditCard;
        this.clear();
    }

    Order.prototype.clear = function () {
        this.number = '';
        this.items = [];
        this.billingAddress = new Compressor.Address;
        this.shippingAddress = new Compressor.Address;
        this.billToShipAddress = true;
        this.itemTotal = 0;
        this.taxTotal = 0;
        this.shipTotal = 0;
        this.adjustmentTotal = 0;
        this.total = 0;
        this.state = null;
        this.shipmentState = null;
        this.shippingRates = [];
        this.shippingRate = null;
        this.token = null;
        return this.loading = false;
    };

    Order.prototype.load = function (data) {
        console.log('test');

        var i, item, j, len, len1, product, products, ref, variant;
        this.clear();
        this.number = data.number;
        this.state = data.state;
        this.shipmentState = data.shipment_state;
        this.itemTotal = Number(data.item_total);
        this.taxTotal = Number(data.tax_total);
        this.shipTotal = Number(data.ship_total);
        this.adjustmentTotal = Number(data.adjustment_total);
        this.total = Number(data.total);
        this.token = data.token;
        this.billToShipAddress = data.use_billing;
        this.adjustments = Compressor.extend(data.adjustments, Compressor.Adjustment);
        this.line_item_adjustments = Compressor.extend(data.line_item_adjustments, Compressor.Adjustment);
        this.shippingRates = [];
        this.completedAt = data.completed_at;
        this.shipmentState = data.shipment_state;
        this.shipments = data.shipments;
        this.payments = data.payments;
        this.creditApplied = data.total_applicable_store_credit;
        this.totalAfterCredit = data.order_total_after_store_credit;
        this.loadRates(data);

        if (data.bill_address) {
            this.billingAddress = Compressor.extend(data.bill_address, Compressor.Address);
        }

        if (data.ship_address) {
            this.shippingAddress = Compressor.extend(data.ship_address, Compressor.Address);
        }

        //products = Compressor.extend(data.products, Compressor.Product);

        if (data.line_items) {
            ref = data.line_items;
            for (i = 0, len = ref.length; i < len; i++) {
                item = ref[i];

                this.items.push({
                    variant: item.variant,
                    flexi_variant_message: item.flexi_variant_message,
                    quantity: item.quantity,
                    price: item.price
                });
            }
        }

        return this;
    };

    Order.prototype.loadRates = function (data) {
        this.shipment = _.last(data.shipments);
        if (this.shipment) {
            this.shippingRates = Compressor.extend(this.shipment.shipping_rates, Compressor.ShippingRate);
            return this.shippingRate = _.find(this.shippingRates, function (rate) {
                return rate.selected;
            });
        } else {
            return this.shippingRate = null;
        }
    };

    Order.prototype.isEmpty = function () {
        return this.items.length === 0;
    };

    Order.prototype.isValid = function () {
        this.shippingAddress.validate();
        this.actualBillingAddress().validate();
        this.creditCard.validate();
        return this.actualBillingAddress().isValid() && this.shippingAddress.isValid() && (this.creditCard.id || this.creditCard.isValid());
    };

    Order.prototype.isInvalid = function () {
        return !this.isValid();
    };

    Order.prototype.totalQuantity = function () {
        return this.items.reduce((function (total, item) {
            return total + item.quantity;
        }), 0);
    };

    Order.prototype.findVariant = function (variantId) {
        var i, item, len, ref, results;
        ref = this.items;
        results = [];
        for (i = 0, len = ref.length; i < len; i++) {
            item = ref[i];
            if (item.variant.id === variantId) {
                results.push(item);
            }
        }
        return results;
    };

    Order.prototype.findVariantForProduct = function (product) {
        var item, variants;
        variants = (function () {
            var i, len, ref, results;
            ref = this.items;
            results = [];
            for (i = 0, len = ref.length; i < len; i++) {
                item = ref[i];
                if (item.variant.product.id === product.id) {
                    results.push(item.variant);
                }
            }
            return results;
        }).call(this);
        if (variants) {
            return variants[0];
        }
    };

    Order.prototype.hasVariant = function (variant) {
        return variant && this.findVariant(variant.id).length > 0;
    };

    Order.prototype.updateTotals = function () {
        return this.total = this.itemTotal + this.adjustmentTotal + this.taxTotal + this.shipTotal;
    };

    Order.prototype.actualBillingAddress = function () {
        if (this.billToShipAddress) {
            return this.shippingAddress;
        } else {
            return this.billingAddress;
        }
    };

    Order.prototype.resetAddresses = function (user) {
        if (!(user && user.addresses.length > 0)) {
            return;
        }
        if (this.shippingAddress.isEmpty()) {
            this.shippingAddress = user.shippingAddress || user.addresses[0];
        }
        if (this.billingAddress.isEmpty()) {
            return this.billingAddress = user.billingAddress || user.addresses[0];
        }
    };

    Order.prototype.resetCreditCard = function (user) {
        if (user && user.creditCards.length > 0) {
            return this.creditCard = _.last(user.creditCards);
        } else {
            return this.creditCard = new Compressor.CreditCard;
        }
    };

    return Order;

})();