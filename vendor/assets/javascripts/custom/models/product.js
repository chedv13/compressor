Compressor.Product = (function () {
    function Product() {
    }

    Product.prototype.init = function () {
        var ref, ref1, self;
        this.images = Compressor.extend(this.master.images, Compressor.Image);
        this.permalink = "#!/products/" + this.slug;
        if (this.variants.length > 0) {
            this.hasVariants = true;
            this.variants = Compressor.extend(this.variants, Compressor.Variant);
        } else {
            this.hasVariants = false;
            this.master = Compressor.extend(this.master, Compressor.Variant);
            this.variants = [this.master];
        }
        if (((ref = this.ad_hoc_option_types) != null ? ref.length : void 0) > 0 || ((ref1 = this.customization_types) != null ? ref1.length : void 0) > 0) {
            this.hasFlexi = true;
        }
        this.image = this.variants[0].images[0];
        if (this.image == null) {
            this.image = this.images[0];
        }
        self = this;
        this.options = {};
        _.each(this.option_types, function (type) {
            return self.options[type.id] = {
                type: type,
                values: {}
            };
        });
        return _.each(this.variants, function (variant) {
            variant.product = self;
            return _.each(variant.option_values, function (value) {
                var option, type;
                type = _.find(self.option_types, function (type) {
                    return type.id === value.option_type_id;
                });
                if (type) {
                    option = self.options[type.id];
                    if (!option.values[value.position]) {
                        option.values[value.position] = {
                            value: value,
                            variants: []
                        };
                    }
                    return option.values[value.position].variants.push(variant);
                }
            });
        });
    };

    Product.prototype.variantForValues = function (selectedValues) {
        return _.find(this.variants, function (variant) {
            return variant.option_values.length === selectedValues.length && _.all(selectedValues, function (selected) {
                    return _.find(variant.option_values, function (value) {
                        return value.id === selected.id;
                    });
                });
        });
    };

    Product.prototype.availableValues = function (selectedValues) {
        var matchingVariants, self, values;
        self = this;
        if (selectedValues.length === 0) {
            return _.map(self.options, function (option) {
                return option.values;
            });
        } else {
            matchingVariants = _.filter(self.variants, function (variant) {
                return _.all(selectedValues, function (selected) {
                    return _.find(variant.option_values, function (value) {
                        return value.id === selected.id;
                    });
                });
            });
            values = _.map(matchingVariants, function (variant) {
                return variant.option_values;
            });
            values = _.flatten(values);
            return _.unique(values);
        }
    };

    Product.prototype.findVariant = function (variant_id) {
        console.log(variant_id);
        console.log(this.variants);

        return _.find(this.variants, function (variant) {
            return variant.id === variant_id;
        });
    };

    Product.prototype.isAvailable = function () {
        if (this.hasVariants) {
            return _.any(this.variants, function (variant) {
                return variant.isAvailable();
            });
        } else {
            return this.master.isAvailable();
        }
    };

    Product.prototype.firstAvailableVariant = function () {
        if (this.variants.length === 1) {
            return this.variants[0];
        } else {
            return _.find(this.variants, function (variant) {
                return variant.isAvailable();
            });
        }
    };

    return Product;
})();