Compressor.Image = (function () {
    function Image() {
    }

    Image.prototype.init = function () {
        this.miniUrl = this.mini_url;
        this.smallUrl = this.small_url;
        this.largeUrl = this.large_url;
        return this.productUrl = this.product_url;
    };

    return Image;

})();