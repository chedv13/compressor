'use strict';
Compressor.CreditCard = (function() {
    CreditCard.TYPE_NAMES = {
        master: 'MasterCard',
        visa: 'Visa',
        amex: 'American Express',
        discover: 'Discover',
        dinersclub: 'Diners Club',
        jcb: 'JCB'
    };

    Validity.define(CreditCard, {
        number: ['required', '_validateCardFormat'],
        month: 'required',
        year: 'required',
        cvc: [
            {
                length: {
                    greaterThan: 2,
                    lessThan: 5
                }
            }
        ]
    });

    function CreditCard() {
        this.number = '';
        this.name = null;
        this.month = null;
        this.year = null;
        this.cvc = null;
        this.type = null;
        this.token = null;
        this.lastDigits = null;
    }

    CreditCard.prototype.init = function() {
        this.id = this.id;
        this.name = this.name;
        this.lastDigits = this.last_digits;
        this.token = this.gateway_payment_profile_id;
        return this.type = this.cc_type;
    };

    CreditCard.prototype.isNew = function() {
        return !(this.token && this.token.length > 0);
    };

    CreditCard.prototype.label = function() {
        return this.constructor.TYPE_NAMES[this.type] + " XXXX-XXXX-XXXX-" + this.lastDigits;
    };

    CreditCard.prototype.determineType = function() {
        return this.type = this.number.match(/^3[47]/) ? 'amex' : this.number.match(/^4/) ? 'visa' : this.number.match(/^5[1-5]/) ? 'master' : this.number.match(/^6(5|011)/) ? 'discover' : this.number.match(/^3(0[0-5]|36|38)/) ? 'dinersclub' : this.number.match(/^(2131|1800|35)/) ? 'jcb' : void 0;
    };

    CreditCard.prototype.same = function(other) {
        return this.id === other.id;
    };

    CreditCard.prototype._validateCardFormat = function() {
        if (!Compressor.Luhn.isValid(this.number)) {
            return 'invalid card number';
        }
    };

    return CreditCard;

})();