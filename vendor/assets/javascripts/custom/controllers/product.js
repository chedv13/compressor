angular.module('compressorApp')
    .controller('ProductCtrl', ['$scope', '$routeParams', 'ProductProvider', function ($scope, $routeParams, ProductProvider) {
        ProductProvider.get({_id: 3053}, function (product) {
            $scope.product = product;
        });
    }]);