angular.module('compressorApp')
    .controller('ServicesCtrl', ['$scope', function ($scope) {
        $scope.$parent.selectedSection = 'services';

        $scope.servicesSections = [
            'Пневмоаудит', 'Подбор оборудования', 'Монтаж пневмомагистрали',
            'Пусконаладочные работы', 'Техническое обслуживание', 'Ремонтные работы',
            'Модернизация оборудования', 'Аренда компрессора'
        ];
    }]);