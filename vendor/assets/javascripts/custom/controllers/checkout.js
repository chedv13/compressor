angular.module('compressorApp')
    .controller('CheckoutCtrl', ['$scope', 'Account', 'Cart', function ($scope, Account, Cart) {
        $scope.currentAccount = Account;

        $scope.deliverySteps = ['address', 'payment', 'confirmation'];
        $scope.deliveryStep = 'address';

        $scope.delivery = {
            address: {},
            payment: {
                type: 'cash'
            },
            user: {}
        };

        $scope.checkPaymentType = function (name) {
            $scope.delivery.payment.type = name;
        };

        $scope.nextStep = function () {
            $scope.deliveryStep = $scope.deliverySteps[_.indexOf($scope.deliverySteps, $scope.deliveryStep) + 1];
        };

        $scope.clearFields = function (model_name) {
            _.each($scope.delivery.address, function (key, val) {

                //console.log($scope.delivery.address[key]);
                //
                //$scope.delivery.address[key] = undefined;
            });
            //
            //$scope.delivery.address.locality = undefined;
            //$scope.delivery.address.street = undefined;

            //$scope.delivery_address_form.$setPristine;
        }
    }]);