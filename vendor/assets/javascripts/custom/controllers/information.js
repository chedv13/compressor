angular.module('compressorApp')
    .controller('InformationCtrl', ['$scope', function ($scope) {
        $scope.$parent.selectedSection = 'info';

        $scope.infoSections = [
            'Теория', 'Практика', 'Каталоги и брошюры',
            'Инструкции', 'Комплектации', ' Консультация специалиста'
        ];
    }]);