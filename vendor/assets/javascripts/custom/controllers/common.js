angular.module('compressorApp')
    .controller('CommonCtrl', ['$scope', 'Cart', 'Account', function ($scope, Cart, Account) {
        $scope.currentCart = Cart.current;

        $scope.sections = ['main', 'catalog', 'services', 'info', 'payment', 'delivery', 'contacts'];
        $scope.selectedSection = 'main';

        $scope.changeTaxonNotClicked = function () {
            $scope.taxonNotClicked = false;
        };
    }]);