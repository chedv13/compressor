angular.module('compressorApp')
    .controller('CartCtrl', ['$scope', 'Cart', function ($scope, Cart) {
        $scope.changeItemQuantity = function (item, delta) {
            item.quantity += delta;

            $scope.changeItemTotal();

            Cart.changeItemQuantity(item, delta, null);
        };

        $scope.updateItemQuantity = function (item) {
            console.log(item.quantity);

            $scope.changeItemTotal();

            Cart.updateItemQuantity(item.variant.id, item.quantity, null);
        };

        $scope.changeItemTotal = function () {
            $scope.currentCart.itemTotal = $scope.currentCart.items.reduce(function (sum, item) {
                return sum + (item.quantity * item.price);
            }, 0.0);

        };
    }]);