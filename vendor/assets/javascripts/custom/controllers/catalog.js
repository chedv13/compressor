angular.module('compressorApp')
    .controller('CatalogCtrl', ['$http', '$scope', '$routeParams', '$timeout', '$q', '$route', 'ProductProvider', 'Cart', 'Account', function ($http, $scope, $routeParams, $timeout, $q, $route, ProductProvider, Cart, Account) {
        $scope.$on('$routeChangeSuccess', function ($event, current) {
            if (current && current.redirectTo === undefined) {
                $scope.limitProductProperties = {};
                $scope.productProperties = {};

                $scope.showLoader = true;
                $scope.showFilters = false;
                $scope.showDisplayManager = false;
                $scope.showProductsPager = false;
                $scope.showProducts = false;

                console.log(Account);

                $scope.applyFiltersAndSort(true);
            }
        });

        $scope.$parent.selectedSection = 'catalog';

        $scope.paginationMaxSize = 5;
        $scope.currentPage = 1;

        $scope.showProductsPagination = false;
        $scope.perPage = 10;
        $scope.perPageForPagination = 10;

        // Задание видов отображения для товвроы
        $scope.viewTypes = ['grid', 'extendedList', 'list'];
        $scope.viewType = 'extendedList';

        // Задание видов сортировки для товаров
        $scope.sortTypes = [
            {
                name: 'amount',
                type: 'field',
                direction: 'ASC',
                label: 'цене, сначала недорогие'
            },
            {
                name: 'amount',
                type: 'field',
                direction: 'DESC',
                label: 'цене, сначала дорогие'
            },
            {
                name: 'name',
                type: 'field',
                direction: 'ASC',
                label: 'названию'
            },
            {
                name: 'performance',
                type: 'property',
                direction: 'ASC',
                label: 'производительности'
            },
            {
                name: 'pressure',
                type: 'property',
                direction: 'ASC',
                label: 'давлению'
            },
            {
                name: 'power',
                type: 'property',
                direction: 'ASC',
                label: 'мощности'
            }
        ];
        $scope.defaultSortType = $scope.sortTypes[0];

        $scope.getFilterValues = function (properties, taxonPermalink) {
            if (taxonPermalink) {
                $scope.getPricesExtreme = $http.get('/api/prices/extreme?permalink=' + taxonPermalink, {cache: false});
            } else {
                $scope.getPricesExtreme = $http.get('/api/prices/extreme', {cache: false});
            }

            if (!_.isEmpty(properties)) {
                var names;

                if (properties.float && properties.int) {
                    names = _.map(properties.float, function (x) {
                            return x.name;
                        }).join(',') + ',' + _.map(properties.int, function (x) {
                            return x.name;
                        }).join(',');
                } else if (properties.float) {
                    names = _.map(properties.float, function (x) {
                        return x.name;
                    }).join(',');
                } else if (properties.int) {
                    names = _.map(properties.int, function (x) {
                        return x.name;
                    }).join(',');
                }

                if (names) {
                    if (taxonPermalink) {
                        $scope.getPropertiesExtreme = $http.get('/api/properties/extreme?names=' + names + '&permalink=' + taxonPermalink, {cache: false});
                    } else {
                        $scope.getPropertiesExtreme = $http.get('/api/properties/extreme?names=' + names, {cache: false});
                    }
                }
            }
        };

        $scope.getProducts = function (properties, params, defaultFilters) {
            if (defaultFilters) {
                var requests = [$scope.getPricesExtreme];

                if ($scope.getPropertiesExtreme) {
                    requests.push($scope.getPropertiesExtreme);
                }

                $q.all(requests).then(function (values) {
                    $scope.limitProductProperties = {
                        name: '',
                        sliderProperties: {
                            'Стоимость': values[0].data
                        }
                    };

                    if (values[1]) {
                        $scope.filteredProductProperties = {};

                        _.each(values[1].data, function (extreme, propertyName) {
                            if (extreme.min == extreme.max) {
                                $scope.filteredProductProperties[propertyName] = extreme;
                            }
                        });

                        if (!_.isEmpty($scope.filteredProductProperties)) {
                            _.extend($scope.limitProductProperties, $scope.filteredProductProperties);
                        }
                    }

                    //$scope.productProperties = jQuery.extend(true, {}, $scope.limitProductProperties);
                    //var s = jQuery.extend(true, {}, $scope.limitProductProperties);

                    $scope.productProperties = {};

                    angular.copy($scope.limitProductProperties, $scope.productProperties);

                    $scope.chunkedProperties = _.chunk(_.map($scope.productProperties.sliderProperties, function (v, k) {
                        return k;
                    }), 2);

                    if ($scope.productProperties) {
                        params = _.extend(params, {
                            'additional_query[name]': $scope.productProperties.name
                        });

                        _.each($scope.productProperties.sliderProperties, function (extreme, propertyName) {
                            if (propertyName == 'Стоимость') {
                                params = _.extend(params, {
                                    'additional_query[amount][max]': $scope.productProperties.sliderProperties['Стоимость'].max,
                                    'additional_query[amount][min]': $scope.productProperties.sliderProperties['Стоимость'].min
                                });
                            } else {
                                var lowerCasePropertyName = propertyName.toLowerCase();

                                params['additional_query[properties][' + lowerCasePropertyName + '][min]'] = extreme.min;
                                params['additional_query[properties][' + lowerCasePropertyName + '][max]'] = extreme.max;
                            }
                        });
                    }

                    $scope.products = ProductProvider.query(params, function () {
                        $scope.showLoader = false;

                        $scope.totalCount = $scope.products.total_count;
                        $scope.perPageForPagination = $scope.perPage;

                        $scope.showFilters = $scope.products.total_count > 1;
                        $scope.showDisplayManager = $scope.products.total_count > 0;
                        $scope.showProductsPager = $scope.products.total_count > 0;
                        $scope.showProductsPagination = $scope.products.total_count > $scope.perPage;
                        $scope.showProducts = $scope.products.total_count > 0;
                    }, function () {
                        $scope.showLoader = false;
                    });
                });
            } else {
                if ($scope.productProperties) {
                    params = _.extend(params, {
                        'additional_query[name]': $scope.productProperties.name
                    });

                    _.each($scope.productProperties.sliderProperties, function (extreme, propertyName) {
                        if (propertyName == 'Стоимость') {
                            params = _.extend(params, {
                                'additional_query[amount][max]': $scope.productProperties.sliderProperties['Стоимость'].max,
                                'additional_query[amount][min]': $scope.productProperties.sliderProperties['Стоимость'].min
                            });
                        } else {
                            var lowerCasePropertyName = propertyName.toLowerCase();

                            params['additional_query[properties][' + lowerCasePropertyName + '][min]'] = extreme.min;
                            params['additional_query[properties][' + lowerCasePropertyName + '][max]'] = extreme.max;
                        }
                    });
                }

                $scope.products = ProductProvider.query(params, function () {
                    $scope.showLoader = false;

                    $scope.totalCount = $scope.products.total_count;
                    $scope.perPageForPagination = $scope.perPage;

                    $scope.showFilters = $scope.products.total_count > 1;
                    $scope.showDisplayManager = $scope.products.total_count > 0;
                    $scope.showProductsPager = $scope.products.total_count > 0;
                    $scope.showProductsPagination = $scope.products.total_count > $scope.perPage;

                    $scope.showProducts = $scope.products.total_count > 0;

                    $scope.showProductsPagination = $scope.products.total_count > $scope.perPage;
                }, function () {
                    $scope.showLoader = false;
                });
            }
        };

        $scope.changePageNumber = function () {
            $scope.applyFiltersAndSort();

            //$scope.showProducts = false;
            //$scope.showLoader = true;

            //var params = {
            //    'order_direction': 'ASC',
            //    'order_name': 'amount',
            //    'order_type': 'field',
            //    'page': $scope.currentPage,
            //    ''
            //};

            //var params;
            //
            //$scope.showProducts = false;
            //$scope.showLoader = true;
            //
            //params = {
            //    'order_direction': $scope.defaultSortType.direction,
            //    'order_name': $scope.defaultSortType.name,
            //    'order_type': $scope.defaultSortType.type,
            //    'page': $scope.currentPage,
            //    'per_page': $scope.perPage,
            //    'properties[float]': 'Performance,Power,Pressure',
            //    'properties[text]': 'Manufacturer'
            //};
            //
            //if ($scope.taxonPermalink) params['taxon[permalink]'] = $scope.taxonPermalink;
            //
            //if ($scope.productProperties) {
            //    params = _.extend(params, {
            //        'additional_query[amount][max]': $scope.productProperties.price.max,
            //        'additional_query[amount][min]': $scope.productProperties.price.min,
            //        'additional_query[name]': $scope.productProperties.name,
            //        'additional_query[properties][performance][min]': $scope.productProperties.performance.min,
            //        'additional_query[properties][performance][max]': $scope.productProperties.performance.max,
            //        'additional_query[properties][power][min]': $scope.productProperties.power.min,
            //        'additional_query[properties][power][max]': $scope.productProperties.power.max,
            //        'additional_query[properties][pressure][min]': $scope.productProperties.pressure.min,
            //        'additional_query[properties][pressure][max]': $scope.productProperties.pressure.max
            //    });
            //}
            //
            //$scope.products = ProductProvider.query(params, function () {
            //    $scope.showLoader = false;
            //    $scope.showProducts = true;
            //}, function () {
            //    $scope.showLoader = false;
            //});
        };

        $scope.applyFiltersAndSort = function (defaultFilters) {
            var params = {
                'order_direction': $scope.defaultSortType.direction,
                'order_name': $scope.defaultSortType.name,
                'order_type': $scope.defaultSortType.type,
                'page': $scope.currentPage
            };

            $scope.showProducts = false;
            $scope.showLoader = true;

            if ($scope.defaultSortType) {
                if ($scope.defaultSortType.direction) params['order_direction'] = $scope.defaultSortType.direction;
                if ($scope.defaultSortType.name) params['order_name'] = $scope.defaultSortType.name;
                if ($scope.defaultSortType.type) params['order_type'] = $scope.defaultSortType.type;
            }

            if ($scope.perPage) params['per_page'] = $scope.perPage;

            if ($routeParams.path) {
                var pathParts = $routeParams.path.split('/');

                $scope.taxonPermalink = $routeParams.path;
                $scope.taxonNotClicked = true;

                if (pathParts.length == 2) {
                    $scope.parentTaxonPermalink = $scope.taxonPermalink;
                } else if (pathParts.length == 3) {
                    $scope.parentTaxonPermalink = pathParts[0] + '/' + pathParts[1];
                }

                params['taxon[permalink]'] = $scope.taxonPermalink;

                $scope.getProductProperties = $http.get('/api/products/properties?taxon_permalink=' + $scope.taxonPermalink, {cache: false});

                $scope.getProductProperties.success(function (data, status, headers, config) {
                    _.each(data.properties, function (properties, dataType) {
                        params['properties[' + dataType + ']'] = _.map(properties, function (property) {
                            return property.name;
                        }).join(',');
                    });

                    if (defaultFilters) {
                        $scope.getFilterValues(data.properties, $scope.taxonPermalink);
                        $scope.getProducts(data.properties, params, true);
                    } else {
                        $scope.getProducts(data.properties, params);
                    }
                }).error(function (data, status, headers, config) {
                    console.log('Error!');

                });
            } else {
                $scope.getProductProperties = $http.get('/api/products/properties', {cache: false});

                $scope.getProductProperties.success(function (data, status, headers, config) {
                    _.each(data.properties, function (properties, dataType) {
                        params['properties[' + dataType + ']'] = _.map(properties, function (property) {
                            return property.name;
                        }).join(',');
                    });

                    if (defaultFilters) {
                        $scope.getFilterValues(data.properties);
                        $scope.getProducts(data.properties, params, true);
                    } else {
                        $scope.getProducts(data.properties, params)
                    }
                }).error(function (data, status, headers, config) {
                    console.log('Error!')
                });
            }
        };

        $scope.changeSortType = function (idx) {
            $scope.defaultSortType = $scope.sortTypes[idx];

            $scope.applyFiltersAndSort();
        };

        $scope.changeViewType = function (idx) {
            $scope.viewType = $scope.viewTypes[idx];
        };

        $scope.clearFilters = function () {
            angular.copy($scope.limitProductProperties, $scope.productProperties);
        };

        $scope.changeTaxonNotClicked = function () {
            $scope.taxonNotClicked = false;
        };

        $scope.addToCart = function (variant_id) {
            console.log(variant_id);

            //$scope.adding = true;
            //
            Cart.addVariant(variant_id, 1, $scope.flexi).success(function () {
                var currentCart = Cart.current;

                $scope.adding = false;

                //$scope.$emit('cart.add', {variant: variant_id, qty: 1, flexi: $scope.flexi});
            });
        }
    }]);