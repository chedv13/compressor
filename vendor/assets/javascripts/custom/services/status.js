Compressor.service("Status", function ($rootScope, $translate) {
    var status;
    status = {
        initialized: false,
        pageTitle: "Home",
        bodyClasses: {
            "default": true
        },
        requestedPath: null,
        httpLoading: false,
        routeChanging: false,
        manuallyLoading: false,
        cachedProducts: [],
        meta: {},
        isLoading: function () {
            return this.manuallyLoading || this.httpLoading || this.routeChanging;
        },
        cacheProduct: function (product) {
            return status.cachedProducts.push(product);
        },
        cacheProducts: function (list) {
            return status.cachedProducts = status.cachedProducts.concat(list);
        },
        findCachedProduct: function (slug) {
            return _.find(status.cachedProducts, function (product) {
                return product.slug === slug;
            });
        },
        setPageTitle: function (translation_key) {
            return $translate(translation_key).then(function (text) {
                return status.pageTitle = text;
            });
        },
        addBodyClass: function () {
            return this._eachClass(arguments, function (classes, klass) {
                return classes[klass] = true;
            });
        },
        removeBodyClass: function () {
            return this._eachClass(arguments, function (classes, klass) {
                return classes[klass] = false;
            });
        },
        toggleBodyClass: function () {
            return this._eachClass(arguments, function (classes, klass) {
                return classes[klass] = !classes[klass];
            });
        },
        _eachClass: function (args, fn) {
            var self;
            self = this;
            return _.each(args, function (klass) {
                return fn(self.bodyClasses, klass);
            });
        }
    };
    $rootScope.$on('$routeChangeSuccess', function () {
        return status.bodyClasses = {
            "default": true
        };
    });
    $rootScope.$watch((function () {
        return status.isLoading();
    }), function (loading) {
        if (loading) {
            status.addBodyClass('loading');
            return $rootScope.$broadcast("loading.start");
        } else {
            status.removeBodyClass('loading');
            return $rootScope.$broadcast("loading.end");
        }
    });
    return status;
});