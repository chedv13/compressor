Compressor.factory('Flash', function ($timeout, $translate) {
    return {
        messages: [],
        add: function (type, translate_key) {
            var self;
            self = this;
            return $translate(translate_key).then(function (translated) {
                var flash;
                flash = {
                    type: type,
                    text: translated
                };
                self.messages.push(flash);
                return self.timeout(flash);
            });
        },
        timeout: function (flash) {
            var self;
            self = this;
            return $timeout((function () {
                return self.remove(flash);
            }), 2500);
        },
        success: function (translate_key) {
            return this.add('success', translate_key);
        },
        info: function (translate_key) {
            return this.add('info', translate_key);
        },
        error: function (translate_key) {
            return this.add('danger', translate_key);
        },
        remove: function (flash) {
            return this.messages = this.messages.filter(function (x) {
                return x !== flash;
            });
        },
        hasMessages: function () {
            return this.messages.length > 0;
        }
    };
});