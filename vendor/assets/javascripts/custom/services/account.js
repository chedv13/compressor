Compressor.service("Account", function ($http, _, $q, Cart, Flash, $translate) {
    var service;
    service = {
        fetched: false,
        isLogged: false,
        isGuest: false,
        init: function () {
            var startupData;
            this.clear();
            startupData = Compressor.startupData;
            if (startupData.User) {
                service.populateAccount(startupData.User);
                return service.fetched = true;
            } else {
                return $http.get('/api/account').success(function (data) {


                    service.populateAccount(data);
                    return service.fetched = true;
                }).error(function (data) {
                    service.isLogged = false;
                    return service.fetched = true;
                });
            }
        },
        reload: function (serializer) {
            if (serializer == null) {
                serializer = 'lite';
            }
            this.fetched = false;
            return $http.get("/api/account?serializer=" + serializer).success(function (data) {
                service.populateAccount(data);
                return service.fetched = true;
            }).error(function (data) {
                return service.isLogged = false;
            });
        },
        populateAccount: function (data) {
            this.user = Compressor.extend(data, Compressor.User);
            if (this.user.current_order) {
                Cart.load(this.user.current_order);
            }
            this.isLogged = true;
            this.email = data.email;
            return this.isGuest = false;
        },
        populateGuestAccount: function (data) {
            this.isGuest = true;
            return this.email = data.email;
        },
        clear: function () {
            this.fetched = false;
            this.user = {};
            this.isLogged = false;
            return this.email = null;
        },
        guestLogin: function (data) {
            var email, params;
            email = data === void 0 ? null : data.email;
            params = {
                'order[email]': email
            };
            return $http.post('/api/cart/guest_login.json', $.param(params)).success(function (data) {
                service.populateGuestAccount(data);
                return Flash.success('app.signed_in');
            }).error(function () {
                return Flash.error('app.signin_failed');
            });
        },
        login: function (data) {
            var params;
            params = {
                'spree_user[email]': data.email,
                'spree_user[password]': data.password
            };
            return $http.post('/spree/login.json', $.param(params)).success(function (data) {
                service.populateAccount(data);
                return Flash.success('app.signed_in');
            }).error(function () {
                return Flash.error('app.signin_failed');
            });
        },
        logout: function () {
            return $http.get('/spree/logout').success(function (data) {
                service.isLogged = false;
                service.clear();
                return Cart.init();
            });
        },
        signup: function (data) {
            var params;
            params = {
                spree_user: data
            };
            return $http.post('/api/account', $.param(params)).success(function (data) {
                return service.populateAccount(data);
            });
        },
        forgotPassword: function (data) {
            var params;
            params = {
                spree_user: data
            };
            return $http.post('/api/passwords', $.param(params)).success(function (data) {
                return service.reload();
            });
        },
        resetPassword: function (data) {
            var params;
            params = {
                spree_user: data
            };
            return $http.put('/api/passwords/' + data.reset_password_token, $.param(params)).success(function (data) {
                return service.reload();
            });
        },
        save: function (data) {
            var params;
            params = {
                spree_user: data.serialize()
            };
            return $http.put('/api/account?serializer=full', $.param(params)).success(function (data) {
                service.populateAccount(data);
                return Flash.success('app.account_updated');
            }).error(function () {
                return Flash.error('app.account_update_failed');
            });
        },
        deleteCard: function (card) {
            var cards;
            cards = this.user.creditCards;
            return $http["delete"]("/api/credit_cards/" + card.id).success(function (data) {
                var i;
                i = cards.indexOf(card);
                if (i !== -1) {
                    return cards.splice(i, 1);
                }
            });
        },
        deleteAddress: function (address) {
            var adds;
            adds = this.user.addresses;
            return $http["delete"]("/api/addresses/" + address.id).success(function (data) {
                var i;
                i = adds.indexOf(address);
                if (i !== -1) {
                    return adds.splice(i, 1);
                }
            });
        }
    };
    service.init();
    return service;
});