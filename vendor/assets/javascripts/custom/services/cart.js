Compressor.service("Cart", function ($http) {
    var service, startupData;

    service = {
        current: null,
        init: function () {
            this.current = new Compressor.Order();

            return this.current;
        },
        reload: function () {
            return $http.get('/api/cart.json').success(this.load);
        },
        errors: function (errors) {
            var attr, attrErrors, key, object, order, parts, results;
            order = service.current;
            order.errors = {};
            order.billingAddress.errors = {};
            order.shippingAddress.errors = {};
            order.creditCard.errors = {};
            results = [];
            for (key in errors) {
                attrErrors = errors[key];
                parts = key.split('.');
                object = parts[0];
                attr = parts[1];
                switch (object) {
                    case 'ship_address':
                        results.push(order.shippingAddress.errors[attr] = attrErrors);
                        break;
                    case 'bill_address':
                        results.push(order.billingAddress.errors[attr] = attrErrors);
                        break;
                    case 'payments':
                        results.push(order.errors.base = attrErrors[0]);
                        break;
                    default:
                        results.push(order.errors[key] = attrErrors);
                }
            }
            return results;
        },
        load: function (data) {
            if (data) {
                return service.current.load(data);
            } else {
                return service.current.clear();
            }
        },
        empty: function () {
            return $http["delete"]('/api/cart').success(this.load);
        },
        addVariant: function (variant_id, quantity, flexi) {
            var params;

            params = {
                variant_id: variant_id,
                quantity: quantity,
                flexi: flexi
            };

            return $http.post('/api/cart/add_variant', params, {
                ignoreLoadingIndicator: true
            }).success(function (response) {
                return service.load(response);
            });
        },
        removeItem: function (item) {
            var i, order;
            order = service.current;
            i = order.items.indexOf(item);
            if (i !== -1) {
                order.items.splice(i, 1);
            }
            return this.updateItemQuantity(item.variant.id, 0);
        },
        changeItemQuantity: function (item, delta, flexi) {
            if (flexi == null) {
                flexi = null;
            }
            if (delta !== 0) {
                return this.updateItemQuantity(item.variant.id, item.quantity + delta, flexi);
            }
        },
        updateItemQuantity: function (id, quantity, flexi) {
            var params;
            if (flexi == null) {
                flexi = null;
            }
            params = {
                variant_id: id,
                quantity: quantity,
                flexi: flexi
            };
            return $http.put('/api/cart/update_variant', params, {
                ignoreLoadingIndicator: true
            })
            //.success(this.load);
        },
        changeVariant: function (oldVariant, newVariant) {
            var params;
            params = $.param({
                old_variant_id: oldVariant.id,
                new_variant_id: newVariant.id
            });
            return $http.put('/api/cart/change_variant', params).success(this.load);
        },
        removeAdjustment: function (adjustment) {
            var params;
            this.current.adjustmentTotal -= adjustment.amount;
            this.current.total -= adjustment.amount;
            this.current.adjustments = _.without(this.current.adjustments, adjustment);
            params = $.param({
                adjustment_id: adjustment.id
            });
            return $http.put('/api/cart/remove_adjustment', params).success(this.load);
        },
        unavailableItems: function () {
            return _.filter(this.current.items, function (item) {
                var variant;
                variant = item.variant;
                return !variant.isAvailable() || (!variant.is_backorderable && variant.total_on_hand < item.quantity);
            });
        },
        clear: function () {
            return this.current.clear();
        },
        totalQuantity: function () {
            return this.current.totalQuantity();
        },
        total: function () {

        },
        findVariant: function (variantId) {
            return this.current.findVariant(variantId);
        },
        hasVariant: function (variant) {
            return this.current.hasVariant(variant);
        },
        isEmpty: function () {
            return this.current.isEmpty();
        }
    };
    service.init();
    startupData = Compressor.startupData;
    if (startupData.Order) {
        service.load(startupData.Order);
    } else {
        service.reload();
    }
    return service;
});